export default interface config_t {
    client_id: string
    token: string
    mongo_url: string
}