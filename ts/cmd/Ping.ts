import { BaseCommandInteraction, Message, MessageEmbed } from 'discord.js'
import { bold, SlashCommandBuilder } from '@discordjs/builders'
import { Command } from '../Command'

const pingCommnand : Command = {
    data: new SlashCommandBuilder()
            .setName('ping')
            .setDescription('Pings the bot and responds with telementary data'), 
    async execute(interaction: BaseCommandInteraction) {
        const date = Date.now()
        const reply = await interaction.reply({content: "Pog", fetchReply: true})

        if (reply instanceof Message) {
            const embed = new MessageEmbed()
            .addFields(
                { name: 'Send', value: (date - interaction.createdAt.getTime()).toString(), inline: true },
                { name: 'Reply', value: (reply.createdAt.getTime() - date).toString(), inline: true },
                { name: 'Total', value: (reply.createdAt.getTime() - interaction.createdAt.getTime()).toString(), inline: true }
            )
            reply.edit({embeds: [embed]})
        }
    }
}

export default pingCommnand;
