import Discord from 'discord.js'
import { Client, VoiceChannel, Intents } from 'discord.js';
import {
	joinVoiceChannel,
	createAudioPlayer,
	createAudioResource,
	entersState,
	StreamType,
	AudioPlayerStatus,
	VoiceConnectionStatus,
} from '@discordjs/voice';
import { createDiscordJSAdapter } from '../adapter';

const player = createAudioPlayer();

async function connectToChannel(channel: VoiceChannel) {
	const connection = joinVoiceChannel({
		channelId: channel.id,
		guildId: channel.guild.id,
		adapterCreator: createDiscordJSAdapter(channel),
	});

	try {
		await entersState(connection, VoiceConnectionStatus.Ready, 30e3);
		return connection;
	} catch (error) {
		connection.destroy();
		throw error;
	}
}

export default async function play(cmd: string, args: string[], msg: Discord.Message) {
	
    if (!msg.guild) return;
    if (msg.attachments.size == 0) return;

	const channel = msg.member?.voice.channel;

	if (channel) {
		try {
			const connection = await connectToChannel(channel);

            const resource = createAudioResource(msg.attachments.first().url, {
                inputType: StreamType.Arbitrary,
            });
        
            player.play(resource);
        
            entersState(player, AudioPlayerStatus.Playing, 5e3);

			connection.subscribe(player);
			msg.reply('Playing now!');
		} catch (error) {
			console.error(error);
		}
	} else {
		msg.reply('Join a voice channel then try again!');
	}
}
