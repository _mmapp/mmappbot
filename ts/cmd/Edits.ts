import Discord from 'discord.js'
import config_t from '../Config'
const config: config_t = require('../../bot.json')

export default async function edits(cmd: string, args: string[], msg: Discord.Message) {

    if (args.length !== 1) {
        msg.reply(`Sorry, but I do not understand! Usage: \`${config.prefix}edits <messageID>\`. Where: \`messageID\` the ID of the message in this channel to be checked.`)
        return;
    }

    try {
        const editedMesage = await msg.channel.messages.fetch(args[0])
        editedMesage.edits.forEach(edit => {
            msg.channel.send(new Discord.MessageEmbed()
                .setAuthor(edit.author.tag, edit.author.avatarURL())
                .setDescription(edit.content)
                .setTimestamp(edit.editedAt)
            )
        })
    } catch (error) {
        msg.reply(`\`Invalid message ID\``)
    }
}
