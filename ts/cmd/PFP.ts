import Discord from 'discord.js'

export default async function PFP(cmd: string, args: string[], msg: Discord.Message) {

    if (msg.mentions.users.size === 1) {
        msg.channel.send(msg.mentions.users.first().avatarURL())
        return
    }

    msg.channel.send(msg.author.avatarURL())
}
