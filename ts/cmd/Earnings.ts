import Discord from 'discord.js'
import User from '../User'
import { findUser } from '../MongoDB'

export default async function earnings(cmd: string, args: string[], msg: Discord.Message) {

    let user = msg.author;

    if (msg.mentions.users.size === 1) {
        user = msg.mentions.users.first()
    }

    if (user.bot) {
	msg.reply("Bots dont have earnings!")
        return;
    }

    let userData: User | null = await findUser(user.id);
    msg.channel.send(`<@${user.id}> has \$${userData ? userData.wallet : 0} earnings.`)
    if (!userData || (userData.wallet === 0)) {
        msg.channel.send("Imagine being broke lol")
    }

}
