import Discord from 'discord.js'
import User from '../User'
import { findUser, updateUser } from '../MongoDB'

export default async function daily(cmd: string, args: string[], msg: Discord.Message) {

    let userData: User | null = await findUser(msg.author.id)
    let daily = Math.round(Math.random() * 100)

    if (!userData) {
        msg.reply(`You got \$${daily}!`)
        updateUser(msg.author.id, { wallet: daily })
        return
    }
    
    if ((Date.now() - userData.lastDaily) < 86400000) {
        msg.reply("You already claimed your daily!")
        return
    }

    msg.reply(`You got \$${daily}!`)
    updateUser(msg.author.id, { wallet: userData.wallet + daily, lastDaily: Date.now() })

}
