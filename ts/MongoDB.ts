import { MongoClient } from 'mongodb';
import user_t from './User'
import config_t from './Config'
const config: config_t = require('../bot.json')
const uri = config.mongo_url

var database, users;

MongoClient.connect(`mongodb://${uri}:27017`, { useNewUrlParser: true, useUnifiedTopology: true }, (err, client) => {
    
    if (err) {
        console.error(err);
        return;
    }
    
    database = client.db("mmappbot");
    users = database.collection<user_t>("users");
});

type UserT = Pick<user_t, "tag">

export async function findUser<UserT>(user: string): Promise<user_t | null> {
    return await users.findOne({ tag: user }, {});
}

export async function updateUser(user: string, data: any) {
    users.updateOne({ tag: user }, { $set: data }, { upsert: true });
}