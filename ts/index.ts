import "core-js/stable";
import "regenerator-runtime/runtime";

import {Client, Intents, Interaction} from 'discord.js'
import config_t from './Config'
import ping_cmd from './cmd/Ping'
import edits_cmd from './cmd/Edits'
import earnings_cmd from './cmd/Earnings'
import daily_cmd from './cmd/Daily'
import pfp_cmd from './cmd/PFP'
import play_cmd from './cmd/Play'
import { Command } from './Command'
import { REST } from "@discordjs/rest";
import { Routes } from "discord-api-types/v9";

const config: config_t = require('../bot.json')

const slashCommands = []

const client = new Client({ intents: [Intents.FLAGS.GUILDS] })
const commandBank = new Map<string, Command>()

commandBank.set('ping', ping_cmd)

for (const command of commandBank) {
    slashCommands.push(command[1].data.toJSON())
}

function alias(aliases: string[], cmd: Command) {
    aliases.forEach(alias => {
        commandBank.set(alias, cmd)
    });
}


client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`)
});

client.on('interactionCreate', async (interaction: Interaction) => {
    if (!interaction.isCommand()) return;
    if (!commandBank.has(interaction.commandName)) return;

    try {
        await commandBank.get(interaction.commandName).execute(interaction)
    } catch (error) {
        interaction.reply(`Command ${interaction.commandName} failed!`)
    }

})

client.login(config.token)

const rest = new REST({ version: '9' })
    .setToken(config.token)
    .put(Routes.applicationCommands(config.client_id), { body: slashCommands })
	.then(() => console.log('Successfully registered application commands.'))
	.catch(console.error)
