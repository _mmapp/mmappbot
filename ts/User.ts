export default interface User {
    tag: string,
    wallet: number,
    lastDaily: number
}