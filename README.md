# mmappbot [![pipeline status](https://gitlab.com/_mmapp/mmappbot/badges/master/pipeline.svg)](https://gitlab.com/_mmapp/mmappbot/-/commits/master)
A Discord bot written in TypeScript.

## Download and Build
Clone the repo: `https://gitlab.com/_mmapp/mmappbot.git`.
Install the dependencies `npm i -D`. *(Node v16.6.0 or newer is required)*
Build: `npm run build`

## Run
Create a `bot.json` file in the root directory and populate it with the following information:
```json
{
	"prefix": string, 
	"token": string,
	"mongo_url": string
}
```
Where:
- `prefix` is your command prefix, e.g.: "?", "!", "pls ", etc. 
- `token` is from https://discord.com/developers/applications/.
- `mongo_url` is the hostname of your MongoDB server.

## Deploy on Docker
1. Create a Docker bridge network: `docker network create mmappbot`.
2. Build the Docker container: `DOCKER_BUILDKIT=1 docker build --tag mmappbot .`.
3. Start a MongoDB container: `docker run -dit --network mmappbot --name mongo mongo:4`.
4. Start a mmappbot container: `docker run -dit --network mmappbot --name mmappbot mmappbot:latest`.
