FROM node:17

ENV NODE_ENV=production

WORKDIR /mmappbot

COPY . .

RUN npm install --production 

CMD ["npm", "start"]
